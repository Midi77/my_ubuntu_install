#!/bin/sh

#PPA'S
add-apt-repository ppa:libreoffice/ppa
add-apt-repository ppa:videolan/stable-daily
add-apt repository ppa:philip5/extra #Digikam & GIMP
add-apt-repository ppa:openshot.developers/ppa #seems obsolote
add-apt-repository ppa:ubuntuhandbook1/audacity

#PPA for stable chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'



echo "updating, upgrading, autocleaning and autoremoving..." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get update   
apt-get -y upgrade   
apt-get -y autoclean   
apt-get -y autoremove  


# removing things from xubuntu
apt-get -y remove abiword
apt-get -y remove gnumeric
apt-get -y remove pidgin


echo "Fixing the backlight thingie"
cd /etc/default/ 
sed -i '11s/.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"/' grub
echo "DONE!"

echo "Fixing the errors"
sed -i '4s/.*/enabled=0/' apport

echo "updating Grub"
update-grub

#installeer Chrome stable  (http://tecadmin.net/install-google-chrome-in-ubuntu/)
echo "installing Chrome Stable...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install google-chrome-stable

# Installeer anti-virus
echo "installing clamav, clamtk and freshclaming...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install clamav 
apt-get -y install clamtk 
freshclam

# Installeer audacity
echo "installing audacity...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install audacity 

# Installeer catfish
echo "installing catfish...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install catfish

# Installeer Clementine:mp3 speler 
echo "installing Clementine...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install clementine 

# Installeer Linux versie van Ccleaner: BleachBit 
echo "installing BleachBit...."
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install BleachBit 

#installeer LibreOffice
echo "installing LibreOffice...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install libreoffice 

#installeer VLC
echo "installing VLC from ppa...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install vlc

#installeer DigiKam
echo "installing DigiKam from ppa...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install digikam

#installeer Gimp
echo "installing Gimp from ppa...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get remove gimp
apt-get -y install gimp

#installeer openshot
echo "installing OpenShot from ppa...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install openshot 


#installeer Lucky Backup, gui for rsync
apt-get -y install luckybackup

# install idle's for python 3.4/2.7
echo "installing idle and idle3...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"  
apt-get -y  install idle &&
apt-get -y install idle3 &&

# install ipython for the two Pythons
echo "installing ipython and ipython3...."
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"  
apt-get -y install ipython 
apt-get -y install ipython3 

# install Kivy (only for Python 2.7)
echo "installing kivy for python 2.7.6...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"  
add-apt-repository ppa:kivy-team/kivy 
apt-get -y update 
apt-get -y install python-kivy 

#Gstreamer Plugins
echo "installing Gstreamer plugins for Kivy sound...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"  
apt-get -y install gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav &&

# Ben Rousch's buildozer install sh for 14.04
# ---------------------------------------------------
echo "installing Ben Rousch's buildozer systemwide...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 

# Install necessary system packages
dpkg --add-architecture i386 
apt-get update 
apt-get -y install build-essential ccache git zlib1g-dev python2.7 python2.7-dev libncurses5:i386 libstdc++6:i386 zlib1g:i386 openjdk-7-jdk unzip &&

# Bootstrap a current Python environment
apt-get remove --purge -y python-virtualenv python-pip python-setuptools
wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - |  python2.7 
rm -f setuptools*.zip
easy_install-2.7 -U pip 
pip2.7 install -U virtualenv

# Install current version of Cython
apt-get remove --purge -y cython 
pip2.7 install cython==0.20.1

# Install Buildozer from master
pip2.7 install -U git+https://github.com/kivy/buildozer.git@master 

#-------------------------------------------------------

# install pip3
echo "installing pip3...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install python-pip 
apt-get -y install python3-pip

# install virtualenv
echo "pip and pip3 installing virtualenv...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
pip install virtualenv
pip3 install virtualenv

# install Python debuggers
echo "installing pudb and ipdb...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
pip install pudb 
pip3 install pudb 
pip install ipdb 
pip3 install ipdb 

# install adb command (for adb logcat/adb devices etc.)
echo "installing android tools adb...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install android-tools-adb

#install checkinstall
echo "installing checkinstall...." 
echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
apt-get -y install checkinstall

# prerequisite for python 2.7.9
#echo "installing libssl-dev, prerequisite for python 2.7.9...." 
#echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
#apt-get install libssl-dev

# create a python 2.7.9 in the home directory, for Django projects
#echo "building python 2.7.9 from source in the home directory ...." 
#echo "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" 
#cd ~ &&
#wget http://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz 
#tar -zxvf Python-2.7.9.tgz &&
#cd Python-2.7.9 &&
#./configure --prefix=/home/tom/Python-2.7.9/ 
#make &&
#checkinstall
#echo "you can remove this with dpkg -r + the name that you gave it in the checkinstall" 


#Avoid permissions errors
chown -R tom:tom ~ 
 


# Non command line things to do

# Settings -> windows Manager -> Keyboard -> Activate Windows tiling with the windows key (set some other things)
# right click workspace and set some left margins and bottom 


