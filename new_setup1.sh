#!/bin/sh

# Fixing the backlight
#
#cd /etc/default/ &&
#sed -i '11s/.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"/' grub &&
#update-grub

# Removing unwanted software from Xubuntu
#
apt-get -y remove abiword &&
apt-get -y remove gnumeric &&
apt-get -y remove pidgin &&
apt-get -y autoremove

# Adding GetDeb and PlayDeb
#
wget http://archive.getdeb.net/install_deb/getdeb-repository_0.1-1~getdeb1_all.deb http://archive.getdeb.net/install_deb/playdeb_0.3-1~getdeb1_all.deb &&
echo "Installing GetDeb" &&
sudo dpkg -i getdeb-repository_0.1-1~getdeb1_all.deb &&
echo "Installing PlayDeb" &&
sudo dpkg -i playdeb_0.3-1~getdeb1_all.deb &&
echo "Deleting Downloads" &&
rm -f getdeb-repository_0.1-1~getdeb1_all.deb &&
rm -f playdeb_0.3-1~getdeb1_all.deb

# PPA's VLC, Digikam, Gimp, gnome3, Libreoffice, PPA Manager, Audacity
#
add-apt-repository -y ppa:videolan/stable-daily &&
add-apt repository -y ppa:philip5/extra #Digikam & GIMP
add-apt-repository -y ppa:gnome3-team/gnome3 &&
add-apt-repository -y ppa:webupd8team/y-ppa-manager &&
add-apt-repository -y ppa:libreoffice/ppa
add-apt-repository -y ppa:ubuntuhandbook1/audacity


# LibDVDCSS for playing DVD's
#
echo 'deb http://download.videolan.org/pub/debian/stable/ /' | sudo tee -a /etc/apt/sources.list.d/libdvdcss.list &&
echo 'deb-src http://download.videolan.org/pub/debian/stable/ /' | sudo tee -a /etc/apt/sources.list.d/libdvdcss.list &&
wget -O - http://download.videolan.org/pub/debian/videolan-apt.asc|sudo apt-key add -


apt-get -y update
apt-get -y upgrade
apt-get -y dist-upgrade


# Install Essentials
# http://howtoubuntu.org/things-to-do-after-installing-ubuntu-14-04-trusty-tahr
apt-get -y install synaptic vlc gimp gimp-data gimp-plugin-registry gimp-data-extras y-ppa-manager bleachbit openjdk-7-jre flashplugin-installer unace unrar zip unzip p7zip-full p7zip-rar sharutils rar uudeview mpack arj cabextract file-roller libxine1-ffmpeg mencoder flac faac faad sox ffmpeg2theora libmpeg2-4 uudeview libmpeg3-1 mpeg3-utils mpegdemux liba52-dev mpeg2dec vorbis-tools id3v2 mpg321 mpg123 libflac++6 totem-mozilla icedax lame libmad0 libjpeg-progs libdvdcss2 libdvdread4 libdvdnav4 libswscale-extra-2 ubuntu-restricted-extras ubuntu-wallpapers*

# LibreOffice
#
apt-get -y install libreoffice

# Audacity
#
apt-get -y install audacity

# Digikam
#
apt-get -y install digikam

# Open Shot
apt-get -y install openshot

# Google Chrome
#
if [[ $(getconf LONG_BIT) = "64" ]]
then
	echo "64bit Detected" &&
	echo "Installing Google Chrome" &&
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb &&
	sudo dpkg -i google-chrome-stable_current_amd64.deb &&
	rm -f google-chrome-stable_current_amd64.deb
else
	echo "32bit Detected" &&
	echo "Installing Google Chrome" &&
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_i386.deb &&
	sudo dpkg -i google-chrome-stable_current_i386.deb &&
	rm -f google-chrome-stable_current_i386.deb
fi

# Skype
#
apt-get -y install skype

# Free File Sync
#
apt-get -y install freefilesync

# Gparted
apt-get -y install gparted

# Calibre
apt-get -y install calibre

#OGM RIP (for dvd's)
apt-get -y install ogm

#Lucky Backup
apt-get -y install luckybackup

# Clam
apt-get -y install clamav 
apt-get -y install clamtk 
freshclam

# Clementine:mp3 speler 
#
apt-get -y install clementine

# install idle's for python 3.4/2.7
#
apt-get -y  install idle &&
apt-get -y install idle3 &&

# install ipython for the two Pythons
#
apt-get -y install ipython 
apt-get -y install ipython3 

# Kivy (only for Python 2.7)
add-apt-repository ppa:kivy-team/kivy &&
apt-get -y update &&
apt-get -y install python-kivy 

#Gstreamer Plugins
#
apt-get -y install gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav

# Ben Rousch's buildozer install sh for 14.04
# 
# Install necessary system packages
dpkg --add-architecture i386 
apt-get update 
apt-get -y install build-essential ccache git zlib1g-dev python2.7 python2.7-dev libncurses5:i386 libstdc++6:i386 zlib1g:i386 openjdk-7-jdk unzip &&

# Bootstrap a current Python environment
apt-get remove --purge -y python-virtualenv python-pip python-setuptools
wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - |  python2.7 
rm -f setuptools*.zip
easy_install-2.7 -U pip 
pip2.7 install -U virtualenv

# Install current version of Cython
apt-get remove --purge -y cython 
pip2.7 install cython==0.20.1

# Install Buildozer from master
pip2.7 install -U git+https://github.com/kivy/buildozer.git@master 

# install pip3
#
apt-get -y install python-pip 
apt-get -y install python3-pip

# virtualenv
pip install virtualenv
pip3 install virtualenv

# install Python debuggers
#
pip install pudb 
pip3 install pudb 
pip install ipdb 
pip3 install ipdb 

# adb command (for adb logcat/adb devices etc.)
#
apt-get -y install android-tools-adb

#install checkinstall
# you can remove a package with dpkg -r + the name that you gave it in the checkinstall"
apt-get -y install checkinstall

#Avoid permissions errors
chown -R tom:tom ~

# Non command line things to do

# Settings -> windows Manager -> Keyboard -> Activate Windows tiling with the windows key (set some other things)
# right click workspace and set some left margins and bottom


